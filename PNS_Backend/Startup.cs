﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(PNS_Backend.Startup))]

namespace PNS_Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}